import Head from "next/head";
import Header from "../components/Header";

export default function Home() {
  return (
    <div>
      <Head>
        <title>facebook</title>
      </Head>

      {/* Header */}
      <Header />
      <main>
        {/* SideBar */}
        {/* Feed */}
        {/* Widgets  */}
      </main>
    </div>
  );
}
